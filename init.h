#ifndef SRC_INIT_H_
#define SRC_INIT_H_
#include "xgpiops.h"
#include "xspips.h"
#include "driver/AD9361/ad9361_api.h"


extern struct ad9361_rf_phy *ad9361_phy;

extern uint32_t* FPGA_Addr;


extern XGpioPs Gpio;
extern XSpiPs Spi;

#define GPIO_AD9361_RST 28
#define GPIO_AD9361_MISC_DEVICE_ID XPAR_GPIO_1_DEVICE_ID
#define GPIO_AD9361_TXNRX    0x00000008
#define GPIO_AD9361_SYNC     0x00000004
#define GPIO_AD9361_ENAGC    0x00000002
#define GPIO_AD9361_ENABLE   0x00000001
#define  GPIO_AD9361_CTRL_DEVICE_ID XPAR_GPIO_0_DEVICE_ID
#define  GPIO_AD9361_CTRLOUT_CHANNEL 1
#define  GPIO_AD9361_CTRLIN_CHANNEL  2
int32_t spi_init();
void gpio_init();
void Transciver_init(float rxFreq,float txFreq, float sps,uint8_t R2T2);
void Bram_init();
#endif /* SRC_INIT_H_ */
