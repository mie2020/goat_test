/***************************************************************************//**
 *   @file   Platform.c
 *   @brief  Implementation of Platform Driver.
 *   @author DBogdan (dragos.bogdan@analog.com)
********************************************************************************
 * Copyright 2013(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/

#include "AD9361_platform.h"

#include "ctype.h"
#include "stdint.h"
#include "util.h"
#include <stdlib.h>  
#include "parameters.h"
#include "AD9361_platform.h"

#include "assert.h"
#define SPI2ASAD9361_CONFIG

/***************************************************************************//**
 * @brief usleep
*******************************************************************************/
//wangfei
/*
static __inline void usleep(unsigned long usleep)
{

}
*/
/***************************************************************************//**
 * @brief spi_init   没有用到
*******************************************************************************/


/***************************************************************************//**
 * @brief spi_read  已经移植
*******************************************************************************/
int32_t spi_read(uint8_t *data,uint8_t bytes_number)
{

	XSpiPs_PolledTransfer(&Spi,data,data,bytes_number);
//  HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
//	HAL_SPI_TransmitReceive(&hspi2,data,data,bytes_number,1000);
//  HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
	return bytes_number;
}

/***************************************************************************//**
 * @brief spi_write_then_read      已经移植
*******************************************************************************/
int spi_write_then_read(struct spi_device *spi,
		const unsigned char *txbuf, unsigned n_tx,
		unsigned char *rxbuf, unsigned n_rx)
{
	static u8 buffer [1000];
	assert (n_tx + n_rx < 1000);
	memcpy(buffer,txbuf,n_tx);
	XSpiPs_PolledTransfer(&Spi,buffer,buffer,n_tx + n_rx);
	memcpy(rxbuf,buffer+n_tx,n_rx);
	return 0;
}

/***************************************************************************//**
 * @brief gpio_init     没有用到
*******************************************************************************/

/***************************************************************************//**
 * @brief gpio_direction 没有用到
*******************************************************************************/
void gpio_direction(uint8_t pin, uint8_t direction)
{

}

/***************************************************************************//**
 * @brief gpio_is_valid  已经移植  这个函数在ad9361_mcs函数中用到了
 而且是判断另外一个GPIO管脚是否有效，这里统一处理成有效了
*******************************************************************************/
bool gpio_is_valid(int number)
{
	switch (number){
		case 0: return true;
		case 1: return false;
		case 2: return false;
		case 3: default:return false;	
	}
	//return 1; //wangfei 强制使用GPIO复位AD9364
	//return 0;
}

/***************************************************************************//**
 * @brief gpio_data
*******************************************************************************/
void gpio_data(uint8_t pin, uint8_t data)
{

}

/***************************************************************************//**
 * @brief gpio_set_value  ad9361_mcs函数中用到了，还没处理。
*******************************************************************************/
void gpio_set_value(unsigned gpio, int value)
{

}
void udelay(unsigned long msecs)
{
	usleep(msecs);
}
	

/***************************************************************************//**
 * @brief mdelay   已经移植
*******************************************************************************/
void mdelay(unsigned long msecs)
{
	usleep(msecs*1000);
}

/***************************************************************************//**
 * @brief msleep_interruptible    没有用到
*******************************************************************************/
unsigned long msleep_interruptible(unsigned int msecs)
{

	return 0;
}

/***************************************************************************//**
 * @brief axiadc_init	ad9361_init，ad9361_set_no_ch_mode函数中用到，
 看着像初始化数字接口部分的，但是是和ad9361对接的fpga部分的
 猜测应该是对的，这个函数里面包含adc初始化和dac初始化两个函数，分别
 对fpga的对应数字接口部分进行了初始化
*******************************************************************************/
void axiadc_init(struct ad9361_rf_phy *phy)
{

}

/***************************************************************************//**
 * @brief axiadc_read
*******************************************************************************/
unsigned int axiadc_read(struct axiadc_state *st, unsigned long reg)
{
	return 0;
}

/***************************************************************************//**
 * @brief axiadc_write  ad9364.c中用到很多
*******************************************************************************/
void axiadc_write(struct axiadc_state *st, unsigned reg, unsigned val)
{

}

/***************************************************************************//**
* @brief axiadc_set_pnsel 函数ad9361_dig_tune中用到
*******************************************************************************/
int axiadc_set_pnsel(struct axiadc_state *st, int channel, enum adc_pn_sel sel)
{
	return 0;
}
