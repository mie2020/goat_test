#pragma once

#include "ad9361_api.h"
#include "ad9361.h"
#define b0 57.83
#define b1 31.16
#define  K  57.0
#define DACREGVALUE  39890
extern int CurrentRefValue ;
extern AD9361_InitParam default_init_param;
extern AD9361_RXFIRConfig rx_fir_config;
extern AD9361_TXFIRConfig tx_fir_config;
