/*
 * init.c
 *
 *  Created on: 2018年5月14日
 *      Author: Administrator
 */


#include "xil_printf.h"
#include "xparameters.h"
#include "xplatform_info.h"
#include "init.h"
#include "driver/AD9361/ad9361_param.h"
#include "xgpio.h"
#include "math.h"
//#include "xclk_wiz_hw.h"
#include "sleep.h"
struct ad9361_rf_phy *ad9361_phy;


XGpioPs Gpio;
XSpiPs Spi;
uint32_t* FPGA_Addr;

XGpio Gpio_ad9361_misc;
XGpio Gpio_ad9361_Ctrl;
int32_t spi_init()
{

	XSpiPs_Config* SpiConfig = XSpiPs_LookupConfig(XPAR_PS7_SPI_0_DEVICE_ID);
	XSpiPs_CfgInitialize(&Spi, SpiConfig, SpiConfig->BaseAddress);
	XSpiPs_SetClkPrescaler(&Spi,XSPIPS_CLK_PRESCALE_32);
	XSpiPs_SetOptions(&Spi,XSPIPS_MANUAL_START_OPTION+XSPIPS_MASTER_OPTION+XSPIPS_FORCE_SSELECT_OPTION+XSPIPS_CLK_PHASE_1_OPTION);
	XSpiPs_SetSlaveSelect(&Spi,0);
	return 0;
}
void gpio_init()
{
	XGpioPs_Config* ConfigPtr=XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr,ConfigPtr->BaseAddr);

	XGpioPs_SetOutputEnablePin(&Gpio, GPIO_AD9361_RST, 1);
	XGpioPs_SetDirectionPin(&Gpio, GPIO_AD9361_RST, 1);//0 for Input Direction, 1 for Output Direction.
	XGpioPs_WritePin(&Gpio, GPIO_AD9361_RST, 1);

	XGpio_Initialize(&Gpio_ad9361_misc, GPIO_AD9361_MISC_DEVICE_ID);
	XGpio_DiscreteWrite(&Gpio_ad9361_misc, 1, GPIO_AD9361_ENAGC+GPIO_AD9361_ENABLE);

	XGpio_Initialize(&Gpio_ad9361_Ctrl, GPIO_AD9361_CTRL_DEVICE_ID);
	XGpio_DiscreteWrite(&Gpio_ad9361_Ctrl, GPIO_AD9361_CTRLIN_CHANNEL, 0);
}
//static float calcCMOSFreq(float sampleRate,uint8_t R2T2){
//	if(R2T2)return sampleRate*2;else return sampleRate*1;
//}
//static void calcWritePLLVCOFreq(float Freq){
//	Xil_AssertNonvoid(Freq >= 10e6 && Freq <=250e6);
//	float fdivMinmax[2] ;//600~1200MHz for -1 speed grade;600~1600MHz for -3
//	fdivMinmax[0] = 600e6 / Freq;
//	fdivMinmax[1] = 1200e6 / Freq;
//	int fdiv = round((fdivMinmax[0] + fdivMinmax[1])/2);
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x200,1+(fdiv<<8));//全局分频为1，PLL倍频按VCO范围计算得到
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x204,0);//PLL倍频不移相
//
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x208,fdiv);//CLK0分频，保持输入输出频率一样
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x20C,90*1000);//CLK0移相90度
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x210,50*1000);//CLK0占空比50%
//
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x214,fdiv);//CLK1分频，保持输入输出频率一样
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x218,0);//CLK1移相0度
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x21c,50*1000);//CLK1占空比50%
//
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x25c,1);//保存到FPGA
//	usleep(250);
//	XClk_Wiz_WriteReg(XPAR_CLK_WIZ_0_BASEADDR,0x25c,3);//保存到FPGA
//}

void Transciver_init(float rxFreq,float txFreq, float sps,uint8_t R2T2){
	ad9361_init(&ad9361_phy, &default_init_param);
	ad9361_en_dis_rx(ad9361_phy,1,1);// 2通道:采样率=时钟频率,4通道:采样率=2*时钟频率
	ad9361_en_dis_tx(ad9361_phy,1,1);
	ad9361_set_rx_gain_control_mode(ad9361_phy,0,RF_GAIN_FASTATTACK_AGC);
	ad9361_set_no_ch_mode(ad9361_phy,R2T2?2:1);
	ad9361_set_tx_lo_freq(ad9361_phy,txFreq);
	ad9361_set_rx_lo_freq(ad9361_phy,rxFreq);
	ad9361_set_tx_sampling_freq (ad9361_phy,sps);
	ad9361_set_rx_sampling_freq (ad9361_phy,sps);
	ad9361_set_rx_rf_bandwidth (ad9361_phy,sps*0.833);
	ad9361_set_tx_rf_bandwidth (ad9361_phy,sps*0.833);
	ad9361_set_tx_attenuation(ad9361_phy,0,20000);
	//calcWritePLLVCOFreq(calcCMOSFreq(sps,R2T2));
}

